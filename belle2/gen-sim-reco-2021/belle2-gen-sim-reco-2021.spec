HEPWL_BMKEXE=belle2-gen-sim-reco-2021-bmk.sh
HEPWL_BMKOPTS="-e 10 -c 1"
HEPWL_BMKDIR=belle2-gen-sim-reco-2021
HEPWL_BMKDESCRIPTION="belle2-gen-sim-reco-2021-bmk"
HEPWL_DOCKERIMAGENAME=belle2-gen-sim-reco-2021-bmk
HEPWL_BMKOS="gitlab-registry.cern.ch/linuxsupport/cc7-base:20211101-1.x86_64"
HEPWL_DOCKERIMAGETAG=ci-v0.4
HEPWL_CVMFSREPOS=belle.cern.ch
