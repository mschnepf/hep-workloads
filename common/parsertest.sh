#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

PARSERTEST_DIR=$(dirname $(readlink -f $0) )
# Input arguments
function usage(){
  echo "Usage: $0 [<workload-directory> (default: .)]"
  exit 1
}
WLDIR="unknown"
if [ "$1" != "" ]; then
  if [ -d $1 ]; then
    cd $1
    WLDIR=$(pwd)
    shift
  else
    echo "ERROR! directory $1 not found"
    usage
  fi
fi
if [ "$1" != "" ]; then usage; fi

# Launch this script from the directory containing parseResults.sh and jobs
echo "Workload directory: $(pwd)"
if [ -f parseResults.sh ]; then
  source ${PARSERTEST_DIR}/parser-driver.sh
  source parseResults.sh # load the parseResults function
else
  echo "ERROR! file $(pwd)/parseResults.sh not found"
  exit 1
fi
if [ ! -d jobs ]; then
  echo "ERROR! directory $(pwd)/jobs not found"
  exit 1
fi
jobsdir=$(pwd)/jobs
echo -e "\nJobs directory: $jobsdir"
ls -l $jobsdir

# The environment variable APP is used by parseResults to name the json file ${APP}_summary.json
# In production (CI, benchmarks, refjobs) APP is equal to <vo>-<workload>
# In this test, set APP to a different name TEST-<vo>-<workload>
APPprod=$(basename $(pwd))
APPtest=TEST-${APPprod}
APP=${APPtest} # this is used by function parseResults

#
# LOOP OVER JOBS
#
summary_message=""
tstatus=0
jobs=$(cd $jobsdir; ls | grep -v refjob)
for job in $jobs; do

  # Examine only the good_1 job (for the moment)
  refdir=$jobsdir/$job
  if [ ! -d ${refdir} ]; then
    echo "ERROR! directory ${refdir} not found"
    exit 1
  fi
  echo -e "\n======================================================="
  echo -e "\nReference job directory: ${refdir}"

  # Define NCOPIES, NTHREADS, NEVENTS_JSON from inputs.log
  if [ -f ${refdir}/inputs.log ]; then
    source ${refdir}/inputs.log
  else
    echo "ERROR! file ${refdir}/inputs.log not found"
    exit 1
  fi

  # Define BMKDIR as the directory containing version.json
  BMKDIR=$refdir
  baseWDir=$refdir

  # Export variables (e.g. for the parseResults.py subprocess)
  DEBUG=1
  #flavor="singularity"
  for var in NCOPIES NTHREADS NEVENTS_THREAD BMKDIR DEBUG APP flavor baseWDir; do
    export $var
  done

  # Parse results and produce the new json
  cd $refdir
  \rm -f ${APPtest}*json version_derived.json parser_output.json
  enrich_version_json
  parseResultsWrapper 0 # > /dev/null # comment out "> /dev/null" to keep debug printouts
  tstatus1=$?

  echo -e "\n parsertest.sh: ls of summaries available"
  ls -l *${APPprod}_summary*

  # Dump the new json and the differences to the old json
  tstatus2=0
  for suffix in _summary.json _summary_new.json _summary_old.json; do
    prodjson=${APPprod}${suffix}
    testjson=${APPtest}${suffix}
    if [ -f ${testjson} ]; then
      echo -e "\n${testjson}:"
      cat ${testjson}
      if [ -f ${prodjson} ]; then
        if [ "$tstatus2" == "" ]; then tstatus2=0; fi
        echo -e "\nDIFFERENCES to ${prodjson}:"
        python3 $PARSERTEST_DIR/json-differ.py ${prodjson} ${testjson}
        #diff ${testjson} ${prodjson}
        if [ $? -eq 0 ]; then
          echo "[None]" 
        else 
          tstatus=$((tstatus+1))
          tstatus2=$((tstatus2+1))
        fi
      else
        echo -e "\nWARNING! reference ${prodjson} not found"
      fi
    fi
  done

  # Clean up and dump test results
  #\rm -f *TEST*json
  job_message="${WLDIR} job '$job' (parse exit code=$tstatus1; test exit code=$tstatus2)"
  echo -e "\nParser test completed for ${job_message}"
  summary_message="${summary_message} \n${job_message}"
done
echo -e "\n======================================================="
echo -e "\nSUMMARY"
echo -e "${summary_message}"
echo -e "\nParser test completed (test exit code=$tstatus)"
echo -e "\n======================================================="
exit $tstatus
