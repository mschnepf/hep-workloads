AliGenerator* GeneratorCustom()
{
  // single muon generation with ad-hoc functions
  
  // set external decayer (needed for AliGenParam)
  TVirtualMCDecayer* decayer = new AliDecayerPythia();
  decayer->SetForceDecay(kAll);
  decayer->Init();
  gMC->SetExternalDecayer(decayer);
  
  // compile the macro (needed to use precompiled functions)
  gSystem->AddIncludePath("-I$ALICE_ROOT/include -I$ALICE_PHYSICS/include");
  if ((gROOT->LoadMacro("GenParamCustomSingle.C+")) != 0) {
    printf("ERROR: cannot find GenParamCustomSingle.C\n");
    abort();
    return;
  }
  
  return GenParamCustomSingle();
}

