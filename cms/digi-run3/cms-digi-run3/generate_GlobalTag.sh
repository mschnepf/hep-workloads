# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.
#
# Run this script to regenerate GlobalTag.db
#

export CMSSW_RELEASE=CMSSW_11_2_0
export VO_CMS_SW_DIR=/cvmfs/cms.cern.ch
source $VO_CMS_SW_DIR/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc900
[[ ! -e ${CMSSW_RELEASE} ]] && scram project CMSSW ${CMSSW_RELEASE}
cd ${CMSSW_RELEASE}
eval `scramv1 runtime -sh`
cd ..
# Add the relevant information using the syntax:
#   conddb -y copy <db> --destdb GlobalTag.db
conddb -y copy 112X_mcRun3_2021_realistic_v13 --destdb GlobalTag.db
conddb -y copy L1TCaloParams_static_CMSSW_9_2_10_2017_v1_8_2_updateHFSF_v6MET --destdb GlobalTag.db
conddb -y copy L1TMuonGlobalParamsPrototype_Stage2v0_hlt --destdb GlobalTag.db
rm -rf ${CMSSW_RELEASE}
