# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

res=0
while [ $res ]; do
  echo "Running CMSSW..."
  OUT=`./cms-digi-run3-bmk.sh -e 10 2> /dev/null | grep 'Parsing' | awk '{print $4}'`
  tag=`grep 'not been found' $OUT | awk -F\" '{print $2}'`
  if [ -z "$tag" ] ; then
    echo "No missing tags!"
    break
  fi
  echo "Tag $tag found to be missing!"
  ./add_tag data/GlobalTag.db $tag >> conddb.log
  res=$?
done
