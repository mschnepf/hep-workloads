HEPWL_BMKEXE=cms-digi-run3-bmk.sh
HEPWL_BMKOPTS="-t 4 -e 20"
HEPWL_BMKDIR=cms-digi-run3
HEPWL_BMKDESCRIPTION="CMS DIGI of ttbar events based on CMSSW_11_2_0"
HEPWL_DOCKERIMAGENAME=cms-digi-run3-bmk
HEPWL_DOCKERIMAGETAG=ci-v0.7
HEPWL_CVMFSREPOS=cms.cern.ch
HEPWL_BMKOS="gitlab-registry.cern.ch/linuxsupport/cc7-base:20211101-1.x86_64"
