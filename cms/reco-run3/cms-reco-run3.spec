HEPWL_BMKEXE=cms-reco-run3-bmk.sh
HEPWL_BMKOPTS="-t 4 -e 25" 
HEPWL_BMKDIR=cms-reco-run3
HEPWL_BMKDESCRIPTION="CMS RECO of ttbar events, based on CMSSW_11_2_0"
HEPWL_DOCKERIMAGENAME=cms-reco-run3-bmk
HEPWL_DOCKERIMAGETAG=ci-v0.6
HEPWL_CVMFSREPOS=cms.cern.ch
HEPWL_BMKOS="gitlab-registry.cern.ch/linuxsupport/cc7-base:20211101-1.x86_64"
