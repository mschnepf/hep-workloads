# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

res=0
dir=`pwd`
while [ $res ]; do
  echo "Running CMSSW..."
  OUT=`./cms-reco-run3-bmk.sh -e 10 2> /dev/null | grep 'workdir is' | awk -F' ' '{print $5}'`
  resdir=`dirname $OUT`
  cd $resdir
  tar zxf $resdir/archive_processes_logs.tgz
  cd $dir
  OUT=$resdir/results/*/proc_1/out_1.log
  tag=`grep 'not been found' $OUT | awk -F\" '{print $2}'`
  if [ -z "$tag" ] ; then
    echo "No missing tags!"
    break
  fi
  echo "Tag $tag found to be missing!"
  ./add_tag data/GlobalTag.db $tag >> conddb.log
  res=$?
done
