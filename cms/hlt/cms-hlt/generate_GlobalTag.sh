# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

export CMSSW_RELEASE=CMSSW_12_1_0
export VO_CMS_SW_DIR=/cvmfs/cms.cern.ch
source $VO_CMS_SW_DIR/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc900
[[ ! -e ${CMSSW_RELEASE} ]] && scram project CMSSW ${CMSSW_RELEASE}
cd ${CMSSW_RELEASE}
eval `scramv1 runtime -sh`
cd ..
conddb -y copy 121X_mcRun3_2021_realistic_v15 --destdb data/GlobalTag.db

rm -rf ${CMSSW_RELEASE}
