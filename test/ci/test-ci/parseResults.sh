# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

parseResultsDir=$(cd $(dirname ${BASH_SOURCE}); pwd) # needed to locate parseResults.py

# Function parseResults must be defined in each benchmark (or in a separate file parseResults.sh)
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# Logfiles have been stored in process-specific working directories <basewdir>/proc_<1...NCOPIES>
# The function is started in the base working directory <basewdir>:
# please store here the overall json summary file for all NCOPIES processes combined
function parseResults(){
  echo "[parseResults] current directory: $(pwd)"
  local failedProc=$1
  #-----------------------
  # Parse results
  #-----------------------
  echo "[parseResults] available log files:" proc_*/out_*.log
  local failedParse=0
  for f in proc_*/out_*.log; do
    echo -e "\n[parseResults] --> Parsing results from $f"
    if ! cat $f; then
      echo "[parseResults] ERROR parsing file $f"
      let "failedParse+=1"
    fi
  done
  local score=1 # hardcoded (dummy)
  [ $failedParse -ne 0 ] && return $failedParse
  #-----------------------
  # Generate json summary
  #-----------------------
  # Generate the summary json
  echo "{\"wl-scores\": {\"gen-sim\": $score}}" > $baseWDir/parser_output.json
  echo "[parseResults]  $baseWDir/parser_output.json"
  cat $baseWDir/parser_output.json
  failedJson=${?}
  #-----------------------
  # Return status
  #-----------------------
  return $failedJson
}
