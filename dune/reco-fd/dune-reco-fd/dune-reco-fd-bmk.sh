#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"

  # This needs openssl-devel RPM

#  ROOTFILE=/cvmfs/dune.osgstorage.org/pnfs/fnal.gov/usr/dune/persistent/stash/ContinuousIntegration/DUNEFD/detsim/prodgenie_nue_dune10kt_1x2x6_detsim_Reference.root
  ROOTFILE=$BMKDIR/data/prodgenie_nue_dune10kt_1x2x6_detsim_Reference.root
  echo "dump setup_dune.sh"
  cat /cvmfs/dune.opensciencegrid.org/products/dune/setup_dune.sh
  echo "END dump"
  source /cvmfs/dune.opensciencegrid.org/products/dune/setup_dune.sh > out_$1.log 2> >(tee -a out_$1.log >&2) && \
   setup dunetpc v09_13_00 -q e19:prof >> out_$1.log 2> >(tee -a out_$1.log >&2) && \
   lar --rethrow-all -n 1 --timing-db time.db \
       -o prodgenie_nue_dune10kt_1x2x6_reco_Current.root \
       --config ci_test_reco_dunefd.fcl \
       $ROOTFILE >> out_$1.log 2> >(tee -a out_$1.log >&2)

  status=${?}
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NCOPIES=$(nproc)
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NEVENTS_THREAD=5

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
