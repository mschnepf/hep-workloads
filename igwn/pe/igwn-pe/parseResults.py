#!/usr/bin/env python3

# Based on belle2 parseResults.py

import os
import json 
import dill
import numpy as np

Scores = []

bilby_label = os.environ['bilby_label']

# Each copy generates an output file
for i in range(0, int(os.environ['NCOPIES'])):
    outdir = "proc_" + str(i + 1) + "/result/"
    with open(outdir+bilby_label+"_dynesty.pickle", "rb") as ff:
        data = dill.load(ff)
        nlikelihood = sum(data['ncall'])

    with open(outdir+bilby_label+"_result.json", "r") as ff:
        jobj = json.load(ff)
        sampling_time = jobj['sampling_time']

    likelihoods_per_sec = nlikelihood/sampling_time
    Scores.append(likelihoods_per_sec)


AvgTotal = np.mean(Scores)
MedTotal = np.median(Scores)
MinTotal = np.min(Scores)
MaxTotal = np.max(Scores)

OutputJSON = {}
OutputJSON['wl-scores'] = {}
OutputJSON['wl-stats'] = {}

OutputJSON['wl-scores']['pe'] = AvgTotal * int(os.environ['NCOPIES'])

OutputJSON['wl-stats']['avg'] = AvgTotal
OutputJSON['wl-stats']['median'] = MedTotal
OutputJSON['wl-stats']['min'] = MinTotal
OutputJSON['wl-stats']['max'] = MaxTotal
OutputJSON['wl-stats']['count'] = int(os.environ['NCOPIES'])

basewdir = os.environ['baseWDir']
OutputFile = open(basewdir+"/parser_output.json", "w")
json.dump(OutputJSON, OutputFile)
OutputFile.close()