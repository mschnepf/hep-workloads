#!/bin/bash

#set -x # enable debug printouts

#set -e # immediate exit on error

# Some variables

export bilby_label="igwn_bilby_pe_bmk"
# The following needs to match the environment name created
# in the Dockerfile.append 'conda create -n ...' command
export bilby_conda_env="igwn-pe-bmk"

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  echo "[doOne ($1)] do one! (process $1 of $NCOPIES)"

  # Configure WL copy
  curdir=`pwd`

  # Execute WL copy
  echo "Executing the following number of threads:"
  echo $NTHREADS

  # Activate the conda environment
  source /opt/conda/etc/profile.d/conda.sh
  conda activate "${bilby_conda_env}"

  # Run this copy of the benchmark
  bilby_pipe_analysis "/bmk/data/config_complete.ini" --outdir $curdir --detectors H1 \
      --detectors L1 --label "${bilby_label}" \
      --data-dump-file "/bmk/data/data_generation.pickle" --sampler dynesty
  status=$?

  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Optional function validateInputArguments may be defined in each benchmark
# If it exists, it is expected to set NCOPIES, NTHREADS, NEVENTS_THREAD
# (based on previous defaults and on user inputs USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS)
# Input arguments: none
# Return value: please return 0 if input arguments are valid, 1 otherwise
# The following variables are guaranteed to be defined: NCOPIES, NTHREADS, NEVENTS_THREAD
# (benchmark defaults) and USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS (user inputs)
function validateInputArguments(){
  if [ "$1" != "" ]; then echo "[validateInputArguments] ERROR! Invalid arguments '$@' to validateInputArguments"; return 1; fi
  echo "[validateInputArguments] validate input arguments"
  # Dummy version: accept user inputs as they are
  if [ "$USER_NCOPIES" != "" ]; then NCOPIES=$USER_NCOPIES; fi
  if [ "$USER_NTHREADS" != "" ]; then NTHREADS=$USER_NTHREADS; fi
  if [ "$USER_NEVENTS_THREAD" != "" ]; then NEVENTS_THREAD=$USER_NEVENTS_THREAD; fi
  # Return 0 if input arguments are valid, 1 otherwise
  return 0
}

# Optional function usage_detailed may be defined in each benchmark
# Input arguments: none
# Return value: none
function usage_detailed(){
  echo "NCOPIES*NTHREADS may be lower or greater than nproc=$(nproc)"
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
# Below copied from cms-gen-sim-bmk.sh
NTHREADS=4
NCOPIES=$(( `nproc` / $NTHREADS ))
NEVENTS_THREAD=1 # This is not a meaningful variable for the IGWN Bilby PE workload
if [ "$NCOPIES" -lt 1 ]; then # when $NTHREADS > nproc
  NCOPIES=1
  NTHREADS=`nproc`
fi

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
