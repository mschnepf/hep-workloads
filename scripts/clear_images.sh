#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

function usage(){
  echo "Usage: $(basename $0) [-r regex|--all]"
  exit 1
}

quiet=0
if [ "$1" == "-q" ]; then
  quiet=1
  shift
fi

if [ "$1" == "" ]; then
  IMAGES=$(docker images | grep '<none>' | awk '{print $3}')
elif [ "$1" == "--all" ]; then
  IMAGES=$(docker images | egrep -v '^(REPOSITORY|gitlab-registry.cern.ch/linuxsupport/(slc6|cc7)-base)' | awk '{print $3}')
  shift
elif [ "$1" == "-r" ] && [ "$2" != "" ] ; then
  # use regular expression passed by $2
  IMAGES=$(docker images | egrep -v '^REPOSITORY' | egrep "$2" | awk '{print $3}')
  shift
  shift
else
  usage
fi

###echo $IMAGES

if [ "$quiet" == "0" ]; then
  docker images
  echo ""
fi

if [ "$IMAGES" != "" ]; then
  echo Clearing images "$IMAGES"
  docker rmi -f $IMAGES
  if [ "$quiet" == "0" ]; then
    echo ""
    docker images
  fi
else
  echo There are no docker images to clear
fi

