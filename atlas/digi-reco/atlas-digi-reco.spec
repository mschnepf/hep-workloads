HEPWL_BMKEXE=atlas-digi-reco-bmk.sh
HEPWL_BMKOPTS="-c 1 -e 5 -t 2"
HEPWL_BMKDIR=atlas-digi-reco
HEPWL_BMKDESCRIPTION="ATLAS Digi Reco based on athena version 21.0.77"
HEPWL_DOCKERIMAGENAME=atlas-digi-reco-bmk
HEPWL_DOCKERIMAGETAG=v2.1
HEPWL_CVMFSREPOS=atlas.cern.ch,atlas-condb.cern.ch,atlas-nightlies.cern.ch,sft.cern.ch
