# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

parseResultsDir=$(cd $(dirname ${BASH_SOURCE}); pwd) # needed to locate parseResults.py

# Function parseResults must be defined in each benchmark (or in a separate file parseResults.sh)
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# Logfiles have been stored in process-specific working directories <basewdir>/proc_<1...NCOPIES>
# The function is started in the base working directory <basewdir>:
# please store here the overall json summary file for all NCOPIES processes combined
function parseResults(){
  echo "[parseResults] current directory: $(pwd)"
  #-----------------------
  # Parse results (python)
  #-----------------------
  # echo "[parseResults] python parser starting"
  # export BASE_WDIR=`pwd`
  # python ${parseResultsDir}/parseResults.py # same directory as parseResults.sh
  # pystatus=$?
  # echo "[parseResults] python parser completed (status=$pystatus)"
  #-----------------------
  # Parse results (bash)
  #-----------------------
  echo "[parseResults] bash parser starting"
  echo "[parseResults] parsing results from" proc_*/log.EVNTtoHITS
  # Parsing Event Throughput: xxxx ev/s
  resJSON=`grep -A1 "INFO Statistics for 'evt'" proc_*/log.EVNTtoHITS | grep "<cpu>" | sed -e "s@[^(]*([[:blank:]]*\([ 0-9\.]*\) +/-.*@\1@" | awk '
    BEGIN{amin=1000000;amax=0;count=0;}
    { if ($1>0) {val=1./(int($1*10.)/10000.); a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val} }
    END{sep=sprintf("%*s", 120, "");gsub(/ /, "*", sep);n=asort(a);
      if (n % 2) {
        median=a[(n + 1) / 2];
      } else {
        median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;
      }; 
printf "\"wl-scores\": {\"sim\": %.4f} , \"wl-stats\": {\"avg\": %.4f, \"median\": %.4f, \"min\": %.4f, \"max\": %.4f, \"count\": %d}",
sum, sum/count, median, amin, amax, count
      }'`
  shstatus=$?
  [ "$shstatus" != "0" ] && return $shstatus
  echo "[parseResults] bash parser completed (status=$shstatus)"
  #-----------------------
  # Generate summary
  #-----------------------
  echo "[parseResults] generate report"
  echo "{$resJSON}" > $baseWDir/parser_output.json  
  #-----------------------
  # Return status
  #-----------------------
  return $shstatus
}
