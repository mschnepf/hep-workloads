# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

import os
import json
import sys
import time
import re
from collections import OrderedDict

if (sys.version_info[0] == 2):
    from commands import getstatusoutput
else: #py3
    from subprocess import getstatusoutput

def get_number(dirname):
    """
    Checks the number of proc_* directory so that these directories can be sorted numerically.
    This fix is needed to avoid random Test-parsers pipeline failures due to different order wrt reference file.
    """
    if dirname.find('_') != -1:
        name, number = os.path.splitext(dirname)[0].split('_')
        return (name, int(number))
    else:
        return dirname

def check(infile,injsonfile,scriptname,appname):
    """
    Checks if run was successful. If successful returns True.
    """

    if not os.access(injsonfile,os.R_OK):
        print ("%s ERROR (%s-bmk): File %s not found!" %(scriptname,appname,injsonfile))
        return False

    try: 
      successfulrun = False
      datafile = open(infile, 'r')
      for line in datafile:
        if 'successful run' in line:
          successfulrun = True
          break

      if successfulrun:
        return True
      else:
        return False
    except IOError:
      print ("%s ERROR (%s-bmk): File %s not found!" %(scriptname,appname,infile))
      return False

def read_values(infile):
    import json
    """
    Read information about CPU, memory and number of processed events from PerfMonMT json file.
    """
    if infile.endswith('tar.gz'):
        (rc,outlog)=getstatusoutput("tar -zvxf "+infile)
        if (rc!=0):
            print ("ERROR Failed to un-tar file %s!" % infile )
            print(outlog)
            sys.exit(1) # the script has to exit
        infile=os.path.split(infile)[1][:-7]


    nbevents, cpuAvg, cpuError, cpuSys, vmemPeak, vmemRSS, vmemSwap, evtMaxCpu, walltime = ([] for i in range(9))
    with open(infile,'r') as f:
       inDict=json.load(f)
       nbevents.append(inDict["summary"]["nEvents"])
       cpu=inDict["summary"]["snapshotLevel"]["Execute"]["dCPU"]
       cpuAvg.append(float( cpu/nbevents[-1]))
       vmemPeak.append(inDict["summary"]["peaks"]["vmemPeak"])
       vmemRSS.append(inDict["summary"]["peaks"]["rssPeak"])
       walltime.append(inDict["summary"]["snapshotLevel"]["Execute"]["dWall"])
       
       #Dummy values to keep downstream code happy
       cpuSys.append(0);
       vmemSwap.append(0);
       evtMaxCpu.append(0);
       cpuError.append(0)
    return nbevents,cpuAvg, cpuError, cpuSys, vmemPeak, vmemRSS, vmemSwap, evtMaxCpu, walltime

    #generate_summary(runstatus, unit,     nevt, scalevalues, scores_formatted, cpus,cpusError, cpusSys, evtMaxCpus, vmems,  RSSs, swaps,  walltimes)
def generate_summary(runstatus_,unitvalue,nevt_,scalevalues_,scores_formatted_,cpus_,cpusError_,cpusSys_,evtMaxCpus_,vmems_,RSSs_,swaps_,walltimes_):
    """
    Generate a dictionary with all the information.
    """
    summary = OrderedDict()
    summary["wl-scores"] = {"sim": float(scalevalues_[0])}
    summary["wl-stats"] =  {"avg":float(scalevalues_[2]), "median":float(scalevalues_[3]), "min":float(scalevalues_[4]), "max":float(scalevalues_[5])}
    summary["events_proc_Athena(MP)"] = [int(val) for val in nevt_] 
    summary["CPU_score"] = {"unit":unitvalue, "score":float(scalevalues_[0]), "weighted_score":float(scalevalues_[1]), "avg":float(scalevalues_[2]), 
                            "median":float(scalevalues_[3]), "min":float(scalevalues_[4]), "max":float(scalevalues_[5]), "score_proc":[float(val) for val in scores_formatted_]}
 # 16-01-2022: Commenting because cpuAvg fails to be always float, causing error in logstash                                          
#    summary["CPU_proc"] = {"cpuAvg":[val for val in cpus_], "cpuError":[val for val in cpusError_], 
#                           "cpuSys":[val for val in cpusSys_], "evtMaxCPU":[val for val in evtMaxCpus_]}
    summary["Memory_proc"] = {"vmem":[val for val in vmems_], "RSS":[val for val in RSSs_], "swap":[val for val in swaps_]}
    summary["walltime_proc"] = [float(val) for val in walltimes_]
    return summary


def save_output(data,appname,scriptname,outfile):
    """
    Save output to a JSON file.
    """
    jsonfile = json.dumps(data)
    fjson = open(outfile,"w")
    fjson.write(jsonfile)
    fjson.close()
    print ("%s INFO (%s-bmk): Summary placed in %s" %(scriptname,appname,outfile))


def main():
    """
    Main function where we parse results from a logfile and save them to JSON. 
    """

    # Exit code of this python script: 0=success, 1=failure (BMK-129)
    pythonstatus = 0
    
    # Environment variables
    basename = os.path.basename(__file__)
    basewdir = os.environ['baseWDir']
    bmkdir   = os.environ['BMKDIR']
    app      = os.environ['APP']
    resJSON  = os.path.join(basewdir,"parser_output.json")

    # Global variables
    injsonfile  = "PerfMonMTSvc_result.json.tar.gz"
    logfile   = "log.AtlasG4Tf"
    scale    = 1000.0
    if scale == 1:
       unit = "evt/ms"
    elif scale == 1000:   
       unit = "evt/s"
    elif scale == 1000000:
       unit = "evt/ks"
    else:
      unit = ""
      print ("%s WARNING (%s-bmk): Scale %i does not have predefined unit! Please define it." %(basename,app,scale))

    # Find logfiles
    dirs = []
    for (dirpath, dirnames, filenames) in os.walk(basewdir):
        dirs.extend([x for x in dirnames if re.match(r"^proc_[0-9]+$",x)])  #BMK-890
        break
 
    # Parse results from logfiles
    #print ("Parsing results from %s" %([os.path.join(basewdir,d,logfile) for d in dirs]))
    runstatus, nevt, scores, scores_wAvg, cpus, cpusError, cpusSys, evtMaxCpus, vmems, RSSs, swaps, walltimes = ([] for i in range(12))
    for d in sorted(dirs, key=get_number):
       logfilepath = os.path.join(basewdir,d,logfile)
       injsonfilepath = os.path.join(basewdir,d,injsonfile)
       print ("Parsing results from %s" %(logfilepath))
       if check(logfilepath,injsonfilepath,basename,app): 
          print ("%s INFO (%s-bmk): Run was successful." %(basename,app)) 
          runstatus.append(1)
       else: 
          print ("%s ERROR (%s-bmk): Run was not successful!" %(basename,app))
          runstatus.append(0)
          pythonstatus = 1
          continue
       nevt_proc, cpu_proc, cpuError_proc, cpuSys_proc, vmem_proc, rss_proc, swap_proc, evtmaxcpu_proc, walltime_proc = read_values(injsonfilepath)
       cpu_wAvg = 0.
       nevt_wAvg = 0.
 
       if len(cpu_proc)==len(cpuError_proc)==len(cpuSys_proc)==len(vmem_proc)==len(rss_proc)==len(swap_proc)==len(walltime_proc)==len(nevt_proc):
         for i in range(len(cpu_proc)):
           if cpu_proc[i] > 0:
             cpus.append(cpu_proc[i])
             scores.append(scale/cpu_proc[i])
             cpusError.append(cpuError_proc[i])
             cpusSys.append(cpuSys_proc[i])
             vmems.append(vmem_proc[i])
             RSSs.append(rss_proc[i])
             swaps.append(swap_proc[i])
             walltimes.append(walltime_proc[i])
             nevt.append(nevt_proc[i])
             nevt_wAvg += int(nevt_proc[i])
             cpu_wAvg += cpu_proc[i]*int(nevt_proc[i])
         if cpu_wAvg > 0:
           scores_wAvg.append(scale*len(evtmaxcpu_proc)*nevt_wAvg/cpu_wAvg)
         for i in range(len(evtmaxcpu_proc)):
           evtMaxCpus.append(evtmaxcpu_proc[i])
       else:
         print ("%s WARNING (%s-bmk): Number of threads is not consistent between different variables in %s! All results from this copy will be excluded." %(basename,app,d))

    scores_sorted = sorted(scores)
    scores_formatted = [ '%.6f' % elem for elem in scores ]

    # Calculate scores
    finalscore = 0.
    finalscore_wAvg = 0.
    avg = 0.
    median = 0.
    minimum = 0.
    maximum = 0.
    n = len(scores)
    if n > 0:
       minimum = min(scores)
       maximum = max(scores)
       for i in range(n):
          finalscore += scores[i]
       for i in range(len(scores_wAvg)):
          finalscore_wAvg += scores_wAvg[i]
       avg = finalscore/n
       if (n % 2 != 0):
          median = scores_sorted[(n+1)//2-1]
       else: 
          median = (scores_sorted[(n//2)-1] + scores_sorted[(n//2)])/2.
    scalevalues = [ '%.6f' %(finalscore), '%.6f' %(finalscore_wAvg), '%.6f' %(avg), '%.6f' %(median), '%.6f' %(minimum), '%.6f' %(maximum) ]

    # Generate summary and save to JSON file
    jsonSummary = generate_summary(runstatus, unit, nevt, scalevalues, scores_formatted, cpus, 
                                   cpusError, cpusSys, evtMaxCpus, vmems, RSSs, swaps, walltimes)
    save_output(jsonSummary,app,basename,resJSON)
    print (jsonSummary)

    # Check if JSON file was created 
    if not os.path.isfile(resJSON):
       print ("%s ERROR (%s-bmk): Something went wrong in parsing the CPU score. File path %s does not exist!" %(basename,app,resJSON))
       pythonstatus = 1

    # Exit code of this python script: 0=success, 1=failure (BMK-129)
    sys.exit(pythonstatus)
    
if '__main__' in __name__:
    main()
