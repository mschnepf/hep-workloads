# Run arguments file auto-generated on Tue Nov  2 14:40:00 2021 by:
# JobTransform: AtlasG4Tf
# Version: $Id: trfExe.py 792052 2017-01-13 13:36:51Z mavogel $
# Import runArgs class
from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'AtlasG4Tf' 

runArgs.maxEvents = 6
runArgs.geometryVersion = 'ATLAS-R2-2016-01-00-01_VALIDATION'
runArgs.runNumber = 407343
runArgs.AMITag = 's3126'
runArgs.DataRunNumber = 284500
runArgs.postInclude = ['RecJobTransforms/UseFrontier.py']
runArgs.preExec = ['simFlags.ReleaseGeoModel=False;from AtlasGeoModel.MuonGMJobProperties import MuonGeometryFlags;MuonGeometryFlags.reset();MuonGeometryFlags.hasSTGC.set_Value_and_Lock(False);MuonGeometryFlags.hasMM.set_Value_and_Lock(False)']
runArgs.firstEvent = 6160001
runArgs.physicsList = 'FTFP_BERT_ATL_VALIDATION'
runArgs.randomSeed = 6163
runArgs.conditionsTag = 'OFLCOND-MC16-SDR-14'
runArgs.truthStrategy = 'MC15aPlus'

 # Input data
runArgs.inputEVNTFile = ['/cvmfs/atlas.cern.ch/repo/benchmarks/hep-workloads/input-data/EVNT.13043099._000859.pool.root.1']
runArgs.inputEVNTFileType = 'EVNT'
runArgs.inputEVNTFileNentries = 10000
runArgs.EVNTFileIO = 'input'

 # Output data
runArgs.outputHITSFile = 'myHITS.pool.root'
runArgs.outputHITSFileType = 'HITS'

 # Extra runargs

 # Extra runtime runargs

 # Literal runargs snippets
