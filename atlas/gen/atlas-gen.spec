HEPWL_BMKEXE=atlas-gen-bmk.sh
HEPWL_BMKOPTS="-e 4"
HEPWL_BMKDIR=atlas-gen
HEPWL_BMKDESCRIPTION="ATLAS Event Generation based on athena version 19.2.5.5"
HEPWL_DOCKERIMAGENAME=atlas-gen-bmk
HEPWL_DOCKERIMAGETAG=v2.1
HEPWL_CVMFSREPOS=atlas.cern.ch,atlas-condb.cern.ch,sft.cern.ch
HEPWL_EXTEND_SFT_SPEC=./sft_spec_custom.txt
