from ProdConf import ProdConf

ProdConf(
  NOfEvents=3,
  HistogramFile='Gauss_00071400_00000089_1.Hist.root',
  DDDBTag='dddb-20170721-3',
  AppVersion='v49r9',
  XMLSummaryFile='summaryGauss_00071400_00000089_1.xml',
  Application='Gauss',
  OutputFilePrefix='00071400_00000089_1',
  RunNumber=7140089,
  XMLFileCatalog='pool_xml_catalog.xml',
  FirstEventNumber=8801,
  CondDBTag='sim-20170721-2-vc-md100',
  OutputFileTypes=['sim'],
)

def myPostConfigActions():
  # Configure (and dump settings for) the message service
  # Need millisecond granularity in timestamps ('.%f')
  from Configurables import MessageSvc
  print 'MessageSvc.Format (old):     "%s"'%MessageSvc().Format
  print 'MessageSvc.timeFormat (old): "%s"'%MessageSvc().timeFormat
  MessageSvc().Format = '%u % F%18W%S%7W%R%T %0W%M' # Gaudi default
  MessageSvc().timeFormat = '%Y-%m-%d %H:%M:%S.%f UTC' # Add '.%f' to Gaudi default
  print 'MessageSvc.Format (new):     "%s"'%MessageSvc().Format
  print 'MessageSvc.timeFormat (new): "%s"'%MessageSvc().timeFormat

from Gaudi.Configuration import *
appendPostConfigAction( myPostConfigActions )
