HEPWL_BMKEXE=lhcb-gen-sim-2021-bmk.sh
HEPWL_BMKOPTS="-c 4 -e 5"
HEPWL_BMKDIR=lhcb-gen-sim-2021
HEPWL_BMKDESCRIPTION="LHCb GEN-SIM-2021 benchmark"
HEPWL_BMKOS="gitlab-registry.cern.ch/linuxsupport/cc7-base:20211101-1.x86_64"
HEPWL_DOCKERIMAGENAME=lhcb-gen-sim-2021-bmk
HEPWL_DOCKERIMAGETAG=ci-v0.4
HEPWL_CVMFSREPOS=lhcb.cern.ch,lhcb-condb.cern.ch
