#!/bin/bash

# Copyright 2019-2021 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  # Configure WL copy
  cat $BMKDIR/prodConf_Gauss_0bmk2021_00000726_1.py | sed -e "s/NOfEvents=3/NOfEvents=${NEVENTS_THREAD}/" > prodConf_Gauss_0bmk2021_00000726_1.py
  # Execute WL copy
  unset -f doOne # workaround for LBCORE-1787 within BMK-166
  strace=""
  ###strace="strace -tt -f -o out_$1_strace0.txt" # optionally produce a strace output
  # Use process substitution to tee stderr both to log and stderr (https://stackoverflow.com/a/692407)
  # Unlike a pipe, this retains the command exit code (no need for 'PIPESTATUS' or 'set -o pipefail')
  ###set -x # assume x was not already in $-
  source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh > out_$1.log 2> >(tee -a out_$1.log >&2) && \
    ${strace} lb-run -c x86_64-centos7-gcc9-opt --use="AppConfig v3r404" --use="DecFiles v31r7" --use="ProdConf" Gauss/v55r1 gaudirun.py -T '$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu3.8-HorExtAngle.py' '$DECFILESROOT/options/10000000.py' '$LBPYTHIA8ROOT/options/Pythia8.py' '$APPCONFIGOPTS/Gauss/Gauss-Upgrade-Baseline-20150522.py' '$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmOpt2.py' '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py' "./prodConf_Gauss_0bmk2021_00000726_1.py" >> out_$1.log 2> >(tee -a out_$1.log >&2)
  ###set +x # assume x was not already in $-
  status=${?}
  ###if [ "$strace" != ""  ]; then cat out_$1_strace0.txt | grep '"/cvmfs' | awk '{p=$0; while(1){ i1=index(p,"\"/cvmfs"); if (i1<=0) break; p=substr(p,i1+1); i2=index(p,"\""); if (substr(p,i2+1,3)!="...") {print substr(p,0,i2-1)}; p=substr(p,i2+1)} }' | sort -u > out_$1_strace1.txt; fi
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# [BMK-616] removing .sim files in addition to root files
function custom_clean_workdir(){
  echo -e "\n[lhcb-gen-sim-2021-bmk: custom_clean_workdir] "
  find ${baseWDir} -type f -name '*.root' -o -name "*.sim*" -delete
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NCOPIES=$(nproc)
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NEVENTS_THREAD=5

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
