from ProdConf import ProdConf

ProdConf(
  NOfEvents=5,
  HistogramFile='Gauss_0bmk2021_00000726_1.Hist.root',
  DDDBTag='dddb-20210617',
  AppVersion='v55r1',
  XMLSummaryFile='summaryGauss_0bmk2021_00000726_1.xml',
  Application='Gauss',
  OutputFilePrefix='0bmk2021_00000726_1',
  RunNumber=2021726,
  XMLFileCatalog='pool_xml_catalog.xml',
  FirstEventNumber=1001,
  CondDBTag='sim-20210617-vc-md100',
  OutputFileTypes=['sim'],
)

def myPostConfigActions():
  # Configure (and dump settings for) the message service
  # Need millisecond granularity in timestamps ('.%f')
  from Configurables import MessageSvc
  print ('MessageSvc.Format (old):     "%s"'%MessageSvc().Format)
  print ('MessageSvc.timeFormat (old): "%s"'%MessageSvc().timeFormat)
  MessageSvc().Format = '%u % F%18W%S%7W%R%T %0W%M' # Gaudi default
  MessageSvc().timeFormat = '%Y-%m-%d %H:%M:%S.%f UTC' # Add '.%f' to Gaudi default
  print ('MessageSvc.Format (new):     "%s"'%MessageSvc().Format)
  print ('MessageSvc.timeFormat (new): "%s"'%MessageSvc().timeFormat)

from Gaudi.Configuration import *
appendPostConfigAction( myPostConfigActions )
