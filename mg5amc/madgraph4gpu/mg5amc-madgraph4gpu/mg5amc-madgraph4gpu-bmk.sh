#!/bin/bash

# Copyright 2019-2021 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  # Temporary workaround (sanity check)
  # The CI nodes became unresponsive and had to be rebooted using NEVTS_THREAD=120 (out of memory?)
  if [ $NEVENTS_THREAD -gt 12 ]; then
    echo "ERROR! Please keep NEVENTS_THREAD <= 12 to avoid resource starvation"
    status=1
    echo "[doOne ($1)] $(date) completed (status=$status)"
    return $status
  fi
  # Test access to the GPU
  nocuda=
  if ! nvidia-smi; then
    echo "ERROR! nvidia-smi failed: cannot access the GPU"
    status=1
    echo "[doOne ($1)] $(date) completed (status=$status)"
    return $status
    ###echo "WARNING! nvidia-smi failed: cannot access the GPU"
    ###nocuda=-nocuda
  fi
  # Set up CUDA
  export CUDA_HOME=/usr/local/cuda-11.4
  export PATH=${CUDA_HOME}/bin:${PATH}
  # Configure WL copy
  procdir=$(pwd)
  download=0
  if [ ! -d /bmk/build/madgraph4gpu ]; then
    echo "[doOne ($1)] $(date) madgraph4gpu not found in /bmk/build/madgraph4gpu: download it from github"
    # Download madgraph4gpu if not yet done
    # NB: this is executed only in the first CI docker run with network access (BMK-779)
    download=1
    # Checkout commit: 23 July 2021 after merging PR #240
    git clone https://github.com/madgraph5/madgraph4gpu.git $procdir/madgraph4gpu
    cd $procdir/madgraph4gpu
    git reset --hard 2af16446a89a8124a5361514d8b785dca72f4cd2
  else
    echo "[doOne ($1)] $(date) madgraph4gpu found in /bmk/build/madgraph4gpu: no need to download it from github"
    cp -dpr /bmk/build/madgraph4gpu $procdir/madgraph4gpu
  fi
  cd $procdir/madgraph4gpu/epoch1/cuda/ee_mumu/SubProcesses/P1_Sigma_sm_epem_mupmum
  cat ../Makefile | sed 's/$(cxx_main) $(testmain)$/$(cxx_main)/' | sed 's/$(warning CUDA_HOME/$(info CUDA_HOME/' > ../Makefile.new
  \mv ../Makefile.new ../Makefile
  cat ./throughput12.sh | sed -e "s/2048 256 12/2048 256 ${NEVENTS_THREAD}/" > ./throughput12.sh.new
  \mv ./throughput12.sh.new ./throughput12.sh
  chmod +x ./throughput12.sh
  # Execute WL copy
  cd $procdir/madgraph4gpu/epoch1/cuda/ee_mumu/SubProcesses/P1_Sigma_sm_epem_mupmum
  source /cvmfs/sft.cern.ch/lcg/releases/gcc/9.2.0/x86_64-centos7/setup.sh > $procdir/out_$1.log 2> >(tee -a $procdir/out_$1.log >&2) \
    && ./throughput12.sh -avxall -inl -flt $nocuda >> $procdir/out_$1.log 2> >(tee -a $procdir/out_$1.log >&2)
  status=${?}
  # Copy madgraph4gpu including newly built binaries to $procdir/build (only in process 1!)
  # The bmk-driver.sh will then copy this to /results/build
  # This is the MAIN_BUILDEXPORTDIR directory from where main.sh then copies data into /bmk/build
  # NB: this is executed only in the first CI docker run with network access (BMK-779)
  if [ "$status" == "0" ] && [ "$1" == "1" ]; then
    if [ "$download" == "1" ]; then
      echo "[doOne ($1)] $(date) copy $procdir/madgraph4gpu to $procdir/build/madgraph4gpu"
      mkdir $procdir/build
      cp -dpr $procdir/madgraph4gpu $procdir/build/madgraph4gpu
    else
      echo "[doOne ($1)] $(date) madgraph4gpu already exists in /bmk/build/madgraph4gpu"
    fi
  fi
  # Clean up
  \rm -rf $procdir/madgraph4gpu
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NCOPIES=$(nproc)
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NEVENTS_THREAD=12 # number of 'iterations', each generating 524k events, per thread

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
