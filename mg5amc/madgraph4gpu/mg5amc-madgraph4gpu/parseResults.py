#!/usr/bin/env python

# Copyright 2019-2021 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#from __future__ import print_function
import os, sys
import numpy as np
import json
from collections import OrderedDict

# Append one benchmark result in a given logfile
def appendBmk(tags, tputs, helinl, lang, fptype, avx, tput):
    if helinl is None : raise Exception( 'ERROR: helinl is None' )
    if lang is None : raise Exception( 'ERROR: lang is None' )
    if fptype is None : raise Exception( 'ERROR: fptype is None' )
    if tput is None : raise Exception( 'ERROR: tput is None' )
    if lang == 'cpp' :
        if avx is None : raise Exception( 'ERROR: avx is None for cpp' )
        tag = 'cpp-%s-inl%d-%s'%(fptype, helinl, avx)
    elif lang == 'cuda' :
        if avx is not None : raise Exception( 'ERROR: avx is not None for cuda' )
        tag = 'cuda-%s-inl%d'%(fptype, helinl)
    else: raise Exception( 'ERROR: lang is neither cpp nor cuda' )
    tags += (tag,)
    tputs += (tput,)
    return tags, tputs

# Parse throughput12.sh output
def parseLogTxt(file, debug=False):
    tags = ()
    tputs = ()
    helinl = None
    lang = None
    fptype = None
    avx = None
    tput = None
    if debug : print('FILE:', file)
    fh = open(file)
    for line in fh.readlines() :
        lline = line.split()
        nlline = len(lline)
        if nlline == 0 : continue
        # Decode helinl and lang
        if lline[0] == 'Process' :
            if lline[nlline-1] == '[inlineHel=0]' : helinl = 0
            elif lline[nlline-1] == '[inlineHel=1]' : helinl = 1
            else: raise Exception( 'ERROR: cannot decode helinl in "%s"'%line )
            if lline[2] == 'EPOCH1_EEMUMU_CPP' : lang='cpp'
            elif lline[2] == 'EPOCH1_EEMUMU_CUDA' : lang='cuda'
            else: raise Exception( 'ERROR: cannot decode lang in "%s"'%line )
            ###print( line, end='' )
            ###print( 'helinl=%d'%helinl )
            ###print( 'helinl=%s'%lang )
        # Decode fptype
        if nlline > 1 and lline[0] == 'FP' and lline[1] == 'precision' :
            if nlline > 3 and lline[3] == 'DOUBLE' : fptype = 'd'
            elif nlline > 3 and lline[3] == 'FLOAT' : fptype = 'f'
            else: raise Exception( 'ERROR: cannot decode fptype in "%s"'%line )
            ###print( line, end='' )
            ###print( 'fptype=%s'%fptype )
        # Decode avx
        if nlline > 2 and lline[0] == 'Internal' and lline[1] == 'loops' and lline[2] == 'fptype_sv' :
            if nlline > 5 and lline[5] == "('none':" : avx = 'none'
            elif nlline > 5 and lline[5] == "('sse4':" : avx = 'sse4'
            elif nlline > 5 and lline[5] == "('avx2':" : avx = 'avx2'
            elif nlline > 5 and lline[5] == "('512y':" : avx = '512y'
            elif nlline > 5 and lline[5] == "('512z':" : avx = '512z'
            else: raise Exception( 'ERROR: cannot decode avx in "%s"'%line )
            ###print( line, end='' )
            ###print( 'avx=%s'%avx )
        # Decode tput
        if lline[0] == 'EvtsPerSec[MECalcOnly]' :
            if nlline > 6 and lline[6] == 'sec^-1' : tput = float(lline[4])/1E6 # units: 10^6 events per second
            else: raise Exception( 'ERROR: cannot decode tput in "%s"'%line )
            ###print( line, end='' )
            ###print( 'tput=%s'%tput )
            # Append bmk and clean up
            accept = True
            ###accept = (helinl==0 and fptype=='d' and avx=='none') # FOR DEBUGGING
            if accept: tags, tputs = appendBmk(tags, tputs, helinl, lang, fptype, avx, tput)
            helinl = None
            lang = None
            fptype = None
            avx = None
            tput = None
    if debug:
        for bmk in zip( tags, tputs ): print( bmk )
    return tags, tputs            

# Parse the full log directory of a benchmarking test
def parseBmkDir(resdir, debug=False) :
    if not os.path.isdir(resdir):
        raise Exception( 'ERROR! unknown directory %s'%resdir )
    resdir=os.path.realpath(resdir)
    if debug: print( 'Iterating over', resdir )
    curlist = os.listdir(resdir)
    curlist.sort()
    all_tputs = [] # [ [cpp-d-inl0-none(1), cpp-d-inl0-sse4(1), ...], [cpp-d-inl0-none(2), cpp-d-inl0-sse4(2), ...], ...]
    for d in curlist:
        if d.find('proc_') != 0 : continue
        #if d != 'proc_1' : continue # TO DEBUG!
        ijob = d[5:]
        ###print( ijob )
        logfile = resdir + '/' + d + '/' + 'out_'+ijob+'.log'
        if not os.path.isfile:
            if debug: print( 'WARNING: ', logfile, 'not found' )
            continue
        if debug: print( 'Parsing', logfile )
        tags, tputs = parseLogTxt(logfile, debug)
        all_tputs.append(tputs)
    if all_tputs == [] : raise Exception( 'ERROR! no logs found in directory %s'%resdir )
    all_tputs = np.transpose(all_tputs) # [ [cpp-d-inl0-none(1), cpp-d-inl0-none(2), ...], [cpp-d-inl0-sse4(1), cpp-d-inl0-sse4(2), ...], ...]
    if debug: print( all_tputs )

    wl_scores = OrderedDict( zip(tags, (round(np.sum(s),4) for s in all_tputs) ) )
    wl_stats = OrderedDict( zip(tags, (getStats(s) for s in all_tputs) ) )

    dic_scores = '"wl-scores" : %s' % (json.dumps(wl_scores))
    if debug: print( dic_scores, end='\n' )
    dic_stats = '"wl-stats" : %s' % (json.dumps(wl_stats))
    if debug: print( dic_stats )
    dic_data = '%s , %s' % (dic_scores, dic_stats)
    
    dic_data = {"wl-scores" : wl_scores,
                "wl-stats" : wl_stats}
    print( json.dumps(dic_data) )
    return dic_data

def getStats(s):
    return {"avg" : round(np.mean(s),4),
            "median" : round(np.median(s),4),
            "min" : round(np.min(s),4),
            "max" : round(np.max(s),4),
            "count" : len(s)
        }

    
#-----------------------------------------------------------------------------------------

# Try the following for a realistic test: python3 -c "from parseResults import *; parseBmkDir('jobs/good_1')" 
if __name__ == '__main__':

    # STANDALONE TESTS
    parseLogTxt('/afs/cern.ch/user/a/avalassi/GPU2020/madgraph4gpu/epoch1/cuda/ee_mumu/SubProcesses/P1_Sigma_sm_epem_mupmum/out_1.log', debug=True)
    ###parseLogTxt('jobs/good_1/proc_1/out_1.log', debug=True)
    ###parseBmkDir('jobs/good_1', debug=True)
