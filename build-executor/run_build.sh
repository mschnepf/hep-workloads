#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -e # immediate exit on error

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\n[run_build.sh] starting at $(date)\n"
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

echo -e "Current directory is $(pwd)\n"

# This script performs manual image builds using the same commands as the
# gitlab CI. It uses the exact same script as the CI (.gitlab-ci.sh), as
# discussed in BMK-50. In order to make this possible, some CI_ variables
# are set before calling that script.

# Set CI_JOB_ID
CI_JOB_ID=$(date +"%Y%m%d-%H%M%S-%N")

# Set CI_JOB_NAME
# (NB: this is also used by the .gitlab-ci.sh script to detect noCI mode)
CI_JOB_NAME=noCI

# Set CI_PROJECT_DIR
CI_PROJECT_DIR=$(cd $(dirname $0); cd ..; pwd)

# Set CI_REGISTRY_IMAGE
gitrepo=$(cd ${CI_PROJECT_DIR}; git config --get remote.origin.url)
gitproj=${gitrepo#*gitlab.cern.ch*/}
if [ "${gitproj}" == "${gitrepo}" ]; then
  echo "ERROR! remote.origin.url '$gitrepo' does not seem to be at gitlab.cern.ch"
  exit 1
fi
gitproj=${gitproj%.git}
CI_REGISTRY_IMAGE=gitlab-registry.cern.ch/${gitproj}

# Usage printout
function usage(){
  echo "Usage:   $0 -s <full path to HEP Workload Spec file> [-e <execution step in main.sh>] [-f (to force rebuild of builder image)]"
  echo "Example: $0 -s $(dirname $(dirname $0))/test/ci/test-ci.spec"
  exit 1
}

# Input parameters
CIENV_HEPWL_SPECFILE="" # no default (will fail)
CIENV_BUILDEVENT="" # no default (will be set in .gitlab-ci.sh)
rebuild=0
while getopts "hs:e:df" o; do
  case ${o} in
    s)
      [ "$OPTARG" != "" ] && CIENV_HEPWL_SPECFILE="$OPTARG"
      ;;
    e)
      [ "$OPTARG" != "" ] && CIENV_BUILDEVENT="$OPTARG"
      ;;
    d)
      set -x # Enable debug printouts
      ;;
    f)
      rebuild=1 #rebuild image builder
      ;;
    h)
      usage
      ;;
  esac
done
if [ "$CIENV_HEPWL_SPECFILE" == "" ]; then usage; fi

# First of all: build the image builder
if [ "$rebuild" == "1" ]; then
  echo -e "*** Build hep-workload-builder\n"
  $(${CI_PROJECT_DIR}/build-executor/docker_command.sh)
  if [ "$?" != "0" ]; then echo "ERROR! docker build failed"; exit 1; fi
fi

# Export all relevant environment variables to the .gitlab-ci.sh subprocess
export CI_JOB_ID
export CI_JOB_NAME
export CI_PROJECT_DIR
export CI_REGISTRY_IMAGE
export CIENV_HEPWL_SPECFILE
export CIENV_BUILDEVENT
echo
for var in CI_JOB_ID CI_JOB_NAME CI_PROJECT_DIR CI_REGISTRY_IMAGE CIENV_HEPWL_SPECFILE CIENV_BUILDEVENT; do
  echo "$var=${!var}"
done
echo

# Delegate the rest to the .gitlab-ci.sh script (BMK-50)
${CI_PROJECT_DIR}/.gitlab-ci.sh build
